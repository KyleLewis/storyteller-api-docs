.. Multiverse X Ardis Storyteller API documentation master file, created by
   sphinx-quickstart on Sat Feb 15 09:59:51 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the Multiverse X Ardis Storyteller API documentation!
================================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Storyteller Endpoints
=====================

Disclaimer, this is *NOT* a REST API (sorry, coming Soon^(tm)). There is a session associated with every call to */start* that is maintained in the server process"s memory. Just letting you know so that you can manage those resources better. I would be sure to call */end* when the user wants to, and also to check against non-200 status responses. If you set up bots in multiple channels they can all call the same server instance, just make sure to provide unique IDs for the sessions.

Happy adventuring!

/start
---------------------
    :methods:
    - POST

    :params:
    - uid (int) (Required): The unique ID you want to associate the resulting session with in future calls to */read*, */roll*, */end*, and */info*.
    - text (str) (Required): The starting prompt for the session. Like AI Dungeon 2, the prompt will be used in addition to recent player actions to seed the model for every response.
    - party (list) (Required): A list of party members, which are dictionaries containing:
    	- character_id (int): A character ID to use when future calls to */read* are made, to indicate which character is speaking.
    	- character_name (string): The character"s name.
    	- character_class (string): The character"s class name. This is unused so far, but could be used for flavor text or stat lookups in the future.
    	- character_pronouns (string): The character"s pronouns for when we do pronoun replacement on GPT2 output, in the form "she/her", "he/him", "they/them". 
    - actions (list) (Optional): A list of actions to associate with the session that can be used to define triggered events (game end) and also optionally to set the difficulty for a particular action. Actions involving killing or player death can only be detected from GPT2, so players cant simply declare victory. The actions take the following keys:
    	- onComplete (str) (Required): The string key of an event trigger to call when the action occurs. Right now, only "END" is supported.
    	- action (str) (Required): A simple description of the action that should trigger the behavior. "You" can be used as a stand-in for any member of the party, and party members can either be the subject of object of the action.
    	- text (str) (Required): The text to append to any GPT2 narrative when the action is detected.
    	- difficulty (int) (Optional): A difficulty value to assign to the action *if* the subjet of the action is a player character. Causes any roll values provided to the */roll* call to be scaled linearly such that the provided *difficulty* behaves like the "barely passes" version of the action, which is by default 10 for most actions. E.g., if you provide a difficulty of 19, and the player rolls a 19 when performing *that* action, you will get the flavor text as if the player rolled a 10: "You just barely do the thing...".
    - pronoun_substitution (boolean as a string "True"/"False") (Optional): Whether or not to replace players name and mentions of 'you' in the flavor texts with the character's name and pronouns. Defaults to using 'you' everywhere which, for now, appears to mesh the best with GPT2.
    	

    :returns:
    - uid (int): The same UID that was provided.

Example params:

.. code-block:: json

	{
	  "uid": 1234,
	  "text": "You and your party find yourself before a large black
	  		   gate. The king of the castle looks down upon you
	  		   and asks your business.",
	  "party": [
	  	{"character_name": "Alice",
	  	 "character_id": 111,
	  	 "character_class": "Paladin",
	  	 "character_pronouns": "she/her"},
	  	{"character_name": "Bob",
	  	 "character_id": 222,
	  	 "character_class": "Wizard",
	  	 "character_pronouns": "he/him"}
	  ],
	  "actions": [
	  	{"action": "You die.",
	  	 "onComplete": "END",
	  	 "text": "Y O U   D I E D"},
	  	{"action": "You kill a dragon",
	  	 "onComplete": "END",
	  	 "text": "Having killed the dragon you return to your home
	  	 		  victorious. The End."},
	  	{"action": "You attack the king",
	  	 "onComplete": "END",
	  	 "difficulty": 2,
	  	 "text": "The kings guards quickly surround you, and you are
	  	 		  executed the following morning. The End."}
	  ],
      "pronoun_substitution": "True"
	}

Example python call:

.. code-block:: python

	import requests
	import json
	auth = ('user_name', 'user_key')
	params = {'uid': 1, 'text': 'SomeText', 'party': {'party dict'}}
	params = json.dumps(params)
	result = requests.post('http://storyteller_url/start',
			   auth=auth, params=params)


/read
---------------------

	Within the context of the corresponding session, read the provided text.
    The reader will attempt to identify actions within the text.

    :methods:
    - POST

    :params:
    - uid (int) (Required): The id of the session that the response corresponds to.
    - character_id (int) (Required): The ID of the player who is speaking.
    - text (str) (Required): What the player said.

    :returns:
    - uid (int): The same UID that was provided
    - action (string): Any action attempt that was discovered within the response, such as "RANGED_ATTACK". Returns "DEFAULT" if no specific action was found. The expected responsability of the caller is that any time an action attempt was discovered, you will follow this call with a call to */roll* that completes the action.
    - response (string): The narrative response to the player's input, including descriptions of their attempt that mention them by name. If no specific action was found, this response also includes the output generated by GPT2.

Example params:

.. code-block:: json

	{
	  "uid": 1234,
	  "text": "I charge the king and attack him with my sword!",
	  "character_id": 111
}


/roll
---------------------

	Following a /read call that generated an action, prompt the GPT2 narrator
    with text relevant to the success or failure of the attempted action. If
    succeeding the action fulfills one of the event triggers provided in the
    original call to /start, then the associated event will be ran as well,
    potentially closing the session if the action had "onComplete": "END".

    :methods:
    - POST

    :params:
    - uid (int) (Required): The id of the session that the response corresponds to.
    - roll_value (int) (Required): The value rolled by the player, in [1, 20]

    :returns:
    - uid (int): The same UID that was provided
    - response (string): The narrative response to the player's input, including descriptions of their attempt and what happens as a result. Includes continued narrative output from the GPT2 model.

Example params:

.. code-block:: json

	{
	  "uid": 1234,
	  "roll_value": 18
	}

/end
---------------------

	Close the session associated with the provided UID when the conversation
    is finished.

    :methods:
    - POST

    :params:
    - uid (int) (Required): The id of the session to be closed.

    :returns:
    - uid (int): The same UID that was provided.

Example params:

.. code-block:: json

	{
	  "uid": 1234,
	}


/info
---------------------

	Retrieve all the party, status, and action trigger information about a session.

    :methods:
    - GET

    :params:
    - uid (int) (Required): The id of the session.

    :returns:
    - info (string): A JSON string representing the session.


Example params:

.. code-block:: json

	{
	  "uid": 1234,
	}